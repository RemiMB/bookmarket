package net.tncy.bookmarket.service;

import net.tncy.bookmarket.data.Book;

import java.util.List;

class BookService {

	public int createBook(String title, String publisher, String isbn) {
		return 0;
	}

	public void deleteBook(int bookId) {
		return ;
	}

	public Book getBookById(int bookId) {
		return null;
	}

	public List<Book> findBookByTitle(String title) {
		return null;
	}

	public List<Book> findBookByAuthor(String author) {
		return null;
	}

	public List<Book> findBookByISBN(String isbn) {
		return null;
	}
}