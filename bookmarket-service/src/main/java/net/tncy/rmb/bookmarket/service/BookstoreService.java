package net.tncy.bookmarket.service;

import net.tncy.bookmarket.data.*;

import java.util.List;

class BookstoreService {
	
	public int createBookstore(String name) {
		return 0;
	}

	public void deleteBookstore(int bookstoreId) {
		return ;
	}

	public Bookstore getBookstoreById(int bookstoreId) {
		return null;
	}

	public List<Bookstore> findBookstoreByName() {
		return null;
	}

	public void renameBookstore(int bookstoreId, String newName) {
		return ;
	}

	public void addBookToBookstore(int bookstoreId, int bookId, int qty, float price) {
		return ;
	}

	public float removeBookToBookstore(int bookstoreId, int bookId, int qty) {
		return 0.0f;
	}

	public List<InventoryEntry> getBookstoreInventory(int bookstoreId) {
		return null;
	}

	public void getBookstoreCatalog(int bookstoreId) {
		return ;
	}
}